import pandas as pd 
from fuzzywuzzy import fuzz
import csv

raw_data = pd.read_csv("data/collaborateur_raw.csv")

linkedin_data = pd.read_csv("data/employee_file.csv")

linkedin_data_tn = pd.read_csv("data/employee_file_tunisia.csv")

## concat to dataframes
lk_data = pd.concat([linkedin_data,linkedin_data_tn], axis=0, join='outer', ignore_index=False, keys=None,
          levels=None, names=None, verify_integrity=False, copy=True)



with open('data/linkedin_mapping.csv', mode='w') as linkedin_mapping_file:
    mapping_writer = csv.writer(linkedin_mapping_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    mapping_writer.writerow(["badge_code","full_name", "link"])
    for index, row in raw_data.iterrows():
        full_name = row['Collaborateur_Prenom'] + " " + row['Collaborateur_Nom']
        lower_full_name = full_name.lower()
        for index_lk, row_lk in lk_data.iterrows():
            if(fuzz.token_set_ratio(lower_full_name,row_lk["full_name"].lower()) > 90):
                mapping_writer.writerow([row['Collaborateur_Badge_Code'], row_lk["full_name"],  row_lk["link"]])
                break





    

