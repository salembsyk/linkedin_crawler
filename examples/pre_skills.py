import pandas as pd
from ast import literal_eval



employees_skills = pd.read_csv("data/employees_skills.csv")

skill_lists = employees_skills["skills"].apply(lambda x: literal_eval(str(x)))

flat_skill_list = [item for sublist in skill_lists for item in sublist]

