import os
import actions
from person import Person
from selenium import webdriver
import time
import csv
import pandas as pd

driver = webdriver.Chrome("./chromedriver")

email = ""
password = ""
actions.login(driver, email, password) # if email and password isnt given, it'll prompt in terminal


linkedin_mapping = pd.read_csv("data/linkedin_mapping.csv")

with open('data/employees_skills.csv', mode='w') as employees_skills:
    skill_writer = csv.writer(employees_skills, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    skill_writer.writerow(["badge_code","full_name", "link","skills"])
    for index, row in linkedin_mapping.iterrows():
        person_link = row["link"]
        person_skill = Person(person_link, driver=driver)
        time.sleep(3)
        if person_skill : 
            print(index)
            print(person_skill)
            skill_writer.writerow([row["badge_code"],row["full_name"], row["link"],person_skill])





