import os
import actions
import time
from selenium import webdriver
import csv
driver = webdriver.Chrome("./chromedriver")

## login
email = ""
password = ""
actions.login(driver, email, password) 

company_url_ = ""

driver.get(company_url_)
time.sleep(5)

numOfPeople =  316

lenOfPage = int(numOfPeople / 12)


lastCount = -1
match = False

while not match:
    lastCount += 1

    time.sleep(3)
    driver.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return "
            "lenOfPage;")
    if lastCount == lenOfPage:
        match = True

profile_name_selector = "org-people-profile-card__profile-title"
profile_link_xpath = "//a[./div[contains(@class,'org-people-profile-card__profile-title t-black lt-line-clamp lt-line-clamp--single-line ember-view')]]"

employees_links = driver.find_elements_by_xpath(profile_link_xpath)



with open('data/.csv', mode='w') as employee_file:
    employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    employee_writer.writerow(["full_name", "link"])
    for single_employee in employees_links :
        link = single_employee.get_attribute("href")
        name = single_employee.text
        employee_writer.writerow([name, link])

driver.close()