import pandas as pd
from ast import literal_eval



skills = pd.read_csv("data/skills.csv")
employees_skills = pd.read_csv("data/employees_skills.csv")


skills_names = skills["Skill"].tolist()
skills_names.insert(0,"Compétence / Collaborateur")

skill_matrix = pd.DataFrame(columns = skills_names)


for index, row in employees_skills.iterrows():
    person_skills = literal_eval(row["skills"])
    skill_matrix.loc[index] = False
    skill_matrix.loc[index]["Compétence / Collaborateur"] = row["badge_code"]
    for skill in person_skills : 
        for index_s, row_s in skills.iterrows():
            if skill in row_s["Alternative"] :
                
                skill_matrix.loc[index][row_s["Skill"]] = True

skill_matrix.to_csv(r'data/skill_martix.csv', index = False)   
        