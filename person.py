import time

def Person(url_,driver):
    driver.get(url_)
    time.sleep(2)

    driver.execute_script(
            "window.scrollTo(0, Math.ceil(document.body.scrollHeight/2));")
    time.sleep(3)
    skills = []

    for skill in driver.find_elements_by_xpath("//span[@class='pv-skill-category-entity__name-text t-16 t-black t-bold']"):
       skills.append(skill.text)

    return skills
